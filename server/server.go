package main

 import (
         "fmt"
         "log"
         "net"
         "strings"
 )

 const (
     JOIN_REQ = "JOIN_REQ"
     WRITE_TO_SERVER = "WRITE_TO_SERVER"
     READ_FROM_SERVER = "READ_FROM_SERVER"
 )

 type Client struct {
   clientID int
   conn net.Conn
}

var clients [2]Client

 func main() {

    listener, err := net.Listen("tcp", "localhost:6000")
    handleError(err)

    defer listener.Close()

    for {
        conn, err := listener.Accept()
        handleError(err)

        var clientIndex int
        if clients[0].conn != nil {
          clientIndex = 1
        } else {
          clientIndex = 0
        }

        client := Client{clientID: clientIndex, conn: conn}
        clients[clientIndex] = client

        go handleTCPConnection(client)
    }

 }

 func handleTCPConnection(client Client) {

    fmt.Println("Handling TCP connection for new client")

    message := readFromClient(client)

    if message == JOIN_REQ {
      if client.clientID == 0 {
          client.conn.Write([]byte(WRITE_TO_SERVER))
      } else {
          client.conn.Write([]byte(READ_FROM_SERVER))
      }

      message = readFromClient(client)

      sendMessageToOtherClient(message, client)

      client.conn.Close()

    }
 }

 func readFromClient(client Client) string {
      buffer := make([]byte, 1024)
      n, err := client.conn.Read(buffer)
      handleError(err)

      message := strings.TrimRight(string(buffer[:n]), "\n")
      fmt.Println("Response from client: ", message)

      return message
 }

 func sendMessageToOtherClient(message string, fromClient Client) {

      messageToSend := []byte(message)

      var otherClient Client

      if fromClient.clientID == clients[0].clientID {
          otherClient = clients[1]
      } else {
          otherClient = clients[0]
      }

      if otherClient.conn != nil {
          otherClient.conn.Write(messageToSend)
      } else {
          fmt.Println("No other clients are connected")
      }
 }

 func handleError(err error) {
    if err != nil {
        log.Fatal(err)
    }
 }
