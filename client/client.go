package main

 import (
         "log"
         "net"
         "fmt"
         "os"
         "bufio"
         "strings"
         "crypto/aes"
	       "crypto/cipher"
	       "encoding/hex"
 )

 const (
     JOIN_REQ = "JOIN_REQ"
     WRITE_TO_SERVER = "WRITE_TO_SERVER"
     PRIVATE_KEY = "6368616e676520746869732070617373776f726420746f206120736563726574"
     NONCE = "64a9433eae7ccceee2fc0eda"
 )

 func main() {

     hostName := "localhost"
     portNum := "6000"
     service := hostName + ":" + portNum

     conn, err := net.Dial("tcp", service)

     handleError(err)

     fmt.Printf("Established connection to %s \n", service)

     defer conn.Close()

     sendToServer(conn, JOIN_REQ)

     response := readFromServer(conn)
     if response == WRITE_TO_SERVER {
        readAndSendMessage(conn)
     } else {
        readMessageFromServer(conn)
     }
 }

 func readFromServer(conn net.Conn) string {

      buffer := make([]byte, 1024)
      n, err := conn.Read(buffer)
      handleError(err)

      text := strings.TrimRight(string(buffer[:n]), "\n")
      fmt.Println("Response from server: ", text)

      return text
 }

 func readMessageFromServer(conn net.Conn) string {

      encryptedMessage := readFromServer(conn)
      decryptedMessage := decryptMessage(encryptedMessage)

      fmt.Println("Decrypted response: ", decryptedMessage)

      return decryptedMessage
 }

 func readAndSendMessage(conn net.Conn) {
     reader := bufio.NewReader(os.Stdin)
     fmt.Print("Enter message: ")
     text, _ := reader.ReadString('\n')

     encryptedMessage := encryptMessage(text)
     sendToServer(conn, encryptedMessage)
 }

 func sendToServer(conn net.Conn, text string) {
     message := []byte(text)
     _, err := conn.Write(message)
     handleError(err)
 }

 func encryptMessage(plaintext string) string {

     key, _ := hex.DecodeString(PRIVATE_KEY)
     nonce, _ := hex.DecodeString(NONCE)

     block, err := aes.NewCipher(key)
     handleError(err)

     aesgcm, err := cipher.NewGCM(block)
     handleError(err)

     ciphertext := aesgcm.Seal(nil, nonce, []byte(plaintext), nil)

     return string(ciphertext)

 }

 func decryptMessage(ciphertext string) string {

     key, _ := hex.DecodeString(PRIVATE_KEY)
     nonce, _ := hex.DecodeString(NONCE)

     block, err := aes.NewCipher(key)
     handleError(err)

     aesgcm, err := cipher.NewGCM(block)
     handleError(err)

     plaintext, err := aesgcm.Open(nil, nonce, []byte(ciphertext), nil)
     handleError(err)

     return string(plaintext)
 }

 func handleError(err error) {
     if err != nil {
         log.Fatal(err)
     }
 }
